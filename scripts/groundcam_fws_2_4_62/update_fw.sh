#!/bin/bash
function read_fw_version()
{
   TRY_MAX_TIMES=10
   while [ $TRY_MAX_TIMES -ne 0 ]
   do
      sleep 1
      current_fw_ver=`./upgrade -v`
      echo "read_fw_version current_fw_ver:"$current_fw_ver
      result=`echo $current_fw_ver | grep -Eo '[0-9]_[0-9]'`
      if [[ $result != "" ]]; then
         return 0
      fi
      let TRY_MAX_TIMES-=1
      echo "try read_fw_version counter:"$TRY_MAX_TIMES
   done
   return 1
}

function validate_file_md5()
{
   filename=$1
   md5_result=`md5sum $filename|cut -d ' ' -f1`
   #get the last field string
   md5_expected=`echo ${filename%.*}| rev| cut -d '_' -f1 | rev`
   if [ "${md5_result}" = "${md5_expected}" ]; then
      return 0
   else
      return 1
   fi
}


FILE_NAME=$1
echo ${FILE_NAME}

#validate file
validate_file_md5 "$FILE_NAME"
if [ $? == 0 ]; then
   echo "MD5 checksum passed."
else
   echo "MD5 checksum error."
   exit
fi

#read current firmware version
read_fw_version

#check version and then upgrade
if [[ "$current_fw_ver" == *"no camera found"* ]]; then
   echo 'no camera found,exit upgrade'
   exit
elif [[ "$current_fw_ver" == *"camera open fail"* ]]; then
   echo 'camera open fail,exit upgrade'
   exit
elif [[ "$current_fw_ver" == *"GetSystemStatus failed"* ]]; then
   echo 'GetSystemStatus failed,exit upgrade'
   exit
elif [[ ${FILE_NAME} == *"$current_fw_ver"* ]]; then
   echo 'latest fws!!'
else
   MAX_TIMES=3
   while [ $MAX_TIMES -ne 0 ]
   do
      ./upgrade -u $FILE_NAME
      #read back agin
      read_fw_version
      echo "new_fw_ver:"$current_fw_ver
      if [[ ${FILE_NAME} == *"$current_fw_ver"* ]]; then
         echo 'upgrade succussfully!!'
         exit
      else
         echo 'upgrade failed!!'
      fi
      sleep 1
      let MAX_TIMES-=1
      echo "try upgrade counter:"$MAX_TIMES     
   done 
fi

