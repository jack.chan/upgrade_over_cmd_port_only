#include <iostream>
#include <stdio.h>
#include <depth_camera_cmd_video.h>
#include <boost/algorithm/string.hpp>

void OnDepthFrame(const DepthFrame *df, void *param)
{
	//std::cout<<"video in..."<<std::endl;
}

int main(int argc, char **argv)
{
	std::string firmware_file_full_name_path;
	bool fw_ver_check = false;
	if (strcmp(argv[1], "-u") == 0)
	{
		firmware_file_full_name_path = argv[2];
	}
	else if (strcmp(argv[1], "-v") == 0)
	{
		fw_ver_check = true;
	}

	DepthCameraCmdVideo cmd_video_port;

	std::vector<std::string> camera_list;
	string cmd_port_name;
	string device_name = "";

	// get the valid camera names
	cmd_video_port.GetDepthCameraList(camera_list);
	//std::cout <<"camera_list:"<<camera_list.size()<<endl;
	for (auto name : camera_list)
	{
		//std::cout <<"name:"<<name<<endl;
		if (name.find("8060") != string::npos)
		{
			device_name = name;
			//std::cout <<"device_name:"<<device_name<<endl;
		}
	}

	if (device_name != "")
	{
		cmd_video_port.SetDepthFrameCallback(OnDepthFrame, nullptr);
		cmd_video_port.SetRxDataCallBack([](const uint8_t *buff, int32_t len, void *) {
			//std::cout << buff << std::endl;
		},
										 nullptr);
		if (cmd_video_port.Open(device_name))
		{
			std::string status;
			if (fw_ver_check)
			{
				if (cmd_video_port.GetSystemStatus(status))
				{
					//std::cout << status << endl;
					std::vector<string> result;
					std::vector<string> result1;
					boost::split(result, status, boost::is_any_of("\n"));
					boost::split(result1, result[2], boost::is_any_of(" "));
					std::string fw_ver=result1[3];
					std::replace(fw_ver.begin(), fw_ver.end(), '.', '_');
					std::cout <<fw_ver<<std::endl;
				}
				else
				{
					std::cout << "GetSystemStatus failed" << std::endl;
				}
				return 0;
			}
			//simulate video in
			std::this_thread::sleep_for(std::chrono::seconds(1));

			//start upgrade ,close twice
			cmd_video_port.VideoControl(false);
			if (cmd_video_port.VideoControl(false) == false)
			{
				std::cout << "close video failed" << std::endl;
				//getchar();
			}
			else
			{

				//change file path here 94af543be8310f44ddd4b39c98b469d7_2_4_62.ifw 257ea00a932c39b726b809e4e38ccfaa_2_3_61.ifw
				if (cmd_video_port.StartUpgrade(firmware_file_full_name_path, "app") == false)
				{
					std::cout << "start upgrade failed" << std::endl;
				}
				else //wait upgrade finish , and output result
				{
					std::cout << "video stoped ,begin upgrade" << std::endl;
					std::future_status future_status;
					future_status = cmd_video_port.mUpgradeFuture.wait_for(std::chrono::seconds(30));

					if (cmd_video_port.mUpgradeFuture.get())
					{
						if (future_status == std::future_status::deferred)
						{
							std::cout << "upgrade deferred\n";
						}
						else if (future_status == std::future_status::timeout)
						{
							std::cout << "upgreade timeout\n";
						}
						else if (future_status == std::future_status::ready)
						{
							std::cout << "upgrade complete!\n";
						}
					}
					else
					{
						std::cout << "upgrade failed. UpgradeProgress = " << cmd_video_port.GetUpgradeProgress() << std::endl;
					}

					std::cout << "mUpgradeProgress value(expect 100) : " << cmd_video_port.GetUpgradeProgress() << std::endl;
				}
			}

			std::cout << "rebooting camera..." << endl;
			cmd_video_port.SystemReboot();
			std::cout << "camera rebooted..." << endl;
			std::cout << "closing cmd port..." << endl;
			cmd_video_port.Close();
		}
		else
		{
			std::cout << "camera open fail" << std::endl;
		}
	}
	else
	{
		std::cout << "no camera found" << std::endl;
	}
	return 0;
}